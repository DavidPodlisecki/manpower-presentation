// SVG LOGO DETECT //
if (Modernizr.svg)
{
  $("#logo").attr("src", "includes/images/white_logo.svg");
  //console.log('changed');
};

$('body').flowtype({
 minimum   : 50,
 maximum   : 1200,
 minFont   : 10,
 maxFont   : 100,
 fontRatio : 35
});

// SECTION ONE
$("#close1").click(function(){

$("#section1").fadeOut( 500, function(){
   $("#header1").delay(400).removeClass("hdSlide");
    $(".bod1").fadeOut();
    $(".bxSlider_wrapper").fadeTo(0,0);
    console.log("working");
} );
$(".bg-fade").fadeTo(0, 0);
$("h1").fadeTo(0, 0);
$(".home_logo").fadeTo(0,0)
$("#home").delay(500).fadeIn( 900, function(){
  buttonHeight();
  $(".bg-fade").fadeTo(0, 1);
  $("h1").fadeTo(0, 1);
  $(".main-wrp").addClass("bounceIn");
  $(".home_logo").fadeTo(200,1)
});

});
var slider = $('.bxslider').bxSlider({
      preloadImages: 'all',
      adaptiveHeight: true,
      onSliderLoad: function(){$(".bxslider").css("visibility", "visible");}
    });
var slider2 = $('.bxslider2').bxSlider({
      preloadImages: 'all',
      adaptiveHeight: true,
      onSliderLoad: function(){$(".bxslider").css("visibility", "visible");}
    });

$("#section1btn").click(function() {
  $("#section1").show();
  $("#home").fadeOut(300, function(){
    $(this).hide()
  });
  $(".main-wrp").removeClass("bounceIn");
  $("#header1").fadeIn().addClass('hdSlide');
  $("#body1").delay (900).fadeIn(400, function(){

   slider.reloadSlider();


    //$(".bxslider").css("visibility", "visible");
    $(".bxSlider_wrapper").delay(800).fadeTo(900, 1);


  }

   );
  function SliderHeight () {
  $(".bxSlider_wrapper").each(function(){
     headerHeight = $("#header1").outerHeight()+75;
     bodyHeight = $(window).height()-headerHeight;

    $('.bxSlider_wrapper').css('height', bodyHeight);
  });
}
SliderHeight();
$(window).resize(function(){
  SliderHeight();
});
function HeaderBodyHeight () {
  $(".header").each(function(){
    headHeight = $("#header1").outerHeight();
    bdyHeight = $(window).height()-headHeight;
    $('.body').css('height', bdyHeight);
    //console.log (headHeight, bdyHeight)
     //console.log (headHeight)
  });
}
 HeaderBodyHeight();
 $(window).resize(function(){
  SliderHeight();
  HeaderBodyHeight();
});
});
// END SECTION ONE


// SECTION TWO
$("#close2").click(function(){
$('html, body').css('overflow' , 'auto');
$("#section2").fadeOut( 500, function(){
   $("#header2").delay(400).removeClass("hdSlide");
    $("#body2").fadeOut();
    $(".bxSlider_wrapper2").fadeTo(0,0);
} );
$(".bg-fade").fadeTo(0, 0);
$("h1").fadeTo(0, 0);
$(".home_logo").fadeTo(0,0)
$("#home").delay(500).fadeIn( 900, function(){
  buttonHeight();
  $(".bg-fade").fadeTo(0, 1);
  $("h1").fadeTo(0, 1);
  $(".main-wrp").addClass("bounceIn");
  $(".home_logo").fadeTo(200,1);
});

});

$("#section2btn").click(function() {
  $("#section2").show();
  $("#home").fadeOut(300, function(){
    $(this).hide()
  });
  $(".main-wrp").removeClass("bounceIn");
  $("#header2").fadeIn().addClass('hdSlide');
  $('html, body').css('overflow', 'hidden');
  $("#body2").delay (900).fadeIn(400, function(){

   slider2.reloadSlider();


    //$(".bxslider").css("visibility", "visible");
  $(".bxSlider_wrapper2").delay(800).fadeTo(900, 1);


  });
   function SliderHeight () {
  $(".bxSlider_wrapper2").each(function(){
     headerHeight = $("#header2").outerHeight();
     bodyHeight = $(window).height()-headerHeight;

    $('.bxSlider_wrapper2').css('height', bodyHeight);
  });
}
SliderHeight();
$(window).resize(function(){
  SliderHeight();
});
function HeaderBodyHeight () {
  $(".header").each(function(){
    headHeight = $("#header2").outerHeight();
    bdyHeight = $(window).height()-headHeight;
    $('.body').css('height', bdyHeight);
    //console.log (headHeight, bdyHeight)
     //console.log (headHeight)
  });
}
 HeaderBodyHeight();
 $(window).resize(function(){
  SliderHeight();
  HeaderBodyHeight();
});
});

// END SECTION TWO

// SECTION THREE
$("#close3").click(function(){
$("#section3").fadeOut( 500, function(){
   $("#header3").delay(400).removeClass("hdSlide");
    $("#body3").fadeOut();
    $("#ben_bx1").removeClass();
    $("#ben_bx2").removeClass();
    $("#ben_bx3").removeClass();
    $("#start-cnt1").delay(400).fadeIn(500);
    $("#bdy-cnt1").fadeOut('fast');
    $("#start-cnt2").delay(400).fadeIn(500);
    $("#bdy-cnt2").fadeOut('fast');
    $("#start-cnt3").delay(400).fadeIn(500);
    $("#bdy-cnt3").fadeOut('fast');

} );
$(".bg-fade").fadeTo(0, 0);
$("h1").fadeTo(0, 0);
$(".home_logo").fadeTo(0,0)
$("#home").delay(500).fadeIn( 900, function(){
  buttonHeight();
  $(".bg-fade").fadeTo(0, 1);
  $("h1").fadeTo(0, 1);
  $(".main-wrp").addClass("bounceIn");
  $(".home_logo").fadeTo(200,1)
});

});

$("#section3btn").click(function() {
  $("#section3").show();
  $("#home").fadeOut(300, function(){
    $(this).hide()
  });
  $(".main-wrp").removeClass("bounceIn");
  $("#header3").fadeIn().addClass('hdSlide');
  $("#body3").delay (900).fadeIn(400);

  function HeaderBodyHeight () {
  $(".header").each(function(){
    headHeight = $("#header3").outerHeight();
    bdyHeight = $(window).height()-headHeight;
    $('.body').css('height', bdyHeight);
    //console.log (headHeight, bdyHeight)
     //console.log (headHeight)
  });
}
 HeaderBodyHeight();
 $(window).resize(function(){

  HeaderBodyHeight();
});

 var $bx1 = $("#ben_bx1");
 var $bx2 = $("#ben_bx2");
 var $bx3 = $("#ben_bx3");
 var $bx4 = $("#ben_bx4");

$( "#ben_bx1" ).click(function() {
  if ($bx1.hasClass("grid20_60")) {
      function bx1ResetClass (){
      $bx1.removeClass("grid20_60") .addClass("grid60_20");
      if ($bx2.hasClass("grid60_20")){$bx2.removeClass("grid60_20"); $bx2.addClass("grid20_60"); $("#start-cnt2").delay(200).fadeIn(500); $("#bdy-cnt2").hide();} else {$bx2.addClass("grid20_60");};
      if ($bx4.hasClass("grid60_20")){$bx4.removeClass("grid60_20"); $bx4.addClass("grid20_60"); $("#start-cnt4").delay(200).fadeIn(500); $("#bdy-cnt4").hide();} else {$bx4.addClass("grid20_60");};
      if ($bx3.hasClass("grid60_20")){$bx3.removeClass("grid60_20"); $bx3.addClass("grid20_60"); $("#start-cnt3 ").delay(200).fadeIn(500); $("#bdy-cnt3").hide();} else {$bx3.addClass("grid20_60");};};
      $("#start-cnt1").hide();
      $("#bdy-cnt1").delay(400).fadeIn(500);
       setTimeout(bx1ResetClass, 100);
  } else if ($("#ben_bx1").hasClass("grid60_20")) {
      function bx1RemoveClass (){
      $bx1.removeClass("grid60_20");
      $bx2.removeClass("grid20_60");
      $bx4.removeClass("grid20_60");
      $bx3.removeClass("grid20_60");};
      $("#start-cnt1").delay(400).fadeIn(500);
      $("#bdy-cnt1").hide();
       setTimeout(bx1RemoveClass, 1);
  } else {
     function bx1AddClass () {
     $bx1.addClass("grid60_20");
     $bx2.addClass("grid20_60");
     $bx3.addClass("grid20_60");
     $bx4.addClass("grid20_60");
     $("#start-cnt1").hide();
     $("#bdy-cnt1").delay(200).fadeIn(900);
    };
     setTimeout(bx1AddClass, 1);
  };

});
$( "#ben_bx2" ).click(function() {
  if ($bx2.hasClass("grid20_60")) {
      function bx2ResetClass (){
      $bx2.removeClass("grid20_60");
      $bx2.addClass("grid60_20");
      if ($bx1.hasClass("grid60_20")){$bx1.removeClass("grid60_20"); $bx1.addClass("grid20_60"); $("#start-cnt1").delay(200).fadeIn(500); $("#bdy-cnt1").hide();} else {$bx1.addClass("grid20_60");};
      if ($bx4.hasClass("grid60_20")){$bx4.removeClass("grid60_20"); $bx4.addClass("grid20_60"); $("#start-cnt4").delay(200).fadeIn(500); $("#bdy-cnt4").hide();} else {$bx4.addClass("grid20_60");};
      if ($bx3.hasClass("grid60_20")){$bx3.removeClass("grid60_20"); $bx3.addClass("grid20_60"); $("#start-cnt3").delay(200).fadeIn(500); $("#bdy-cnt3").hide();} else {$bx3.addClass("grid20_60");};};
      $("#start-cnt2").hide();
      $("#bdy-cnt2").delay(200).fadeIn(300);
      setTimeout(bx2ResetClass, 1);
  } else if ($("#ben_bx2").hasClass("grid60_20")) {
      function bx2RemoveClass() {
      $bx1.removeClass("grid20_60");
      $bx2.removeClass("grid60_20");
      $bx4.removeClass("grid20_60");
      $bx3.removeClass("grid20_60");};
      $("#start-cnt2").delay(200).fadeIn(500);
      $("#bdy-cnt2").hide();
      setTimeout(bx2RemoveClass, 1);
  } else {
     function bx2AddClass (){
     $bx1.addClass("grid20_60");
     $bx2.addClass("grid60_20");
     $bx3.addClass("grid20_60");
     $bx4.addClass("grid20_60");
     $("#start-cnt2").hide();
     $("#bdy-cnt2").delay(200).fadeIn(900);
   };
     setTimeout(bx2AddClass, 1);
  };
});
$( "#ben_bx3" ).click(function() {
  if ($bx3.hasClass("grid20_60")) {
      function bx3ResetClass (){
      $bx3.removeClass("grid20_60");
      $bx3.addClass("grid60_20");
      if ($bx1.hasClass("grid60_20")){$bx1.removeClass("grid60_20"); $bx1.addClass("grid20_60"); $("#start-cnt1").delay(200).fadeIn(500); $("#bdy-cnt1").hide();} else {$bx1.addClass("grid20_60");};
      if ($bx4.hasClass("grid60_20")){$bx4.removeClass("grid60_20"); $bx4.addClass("grid20_60"); $("#start-cnt4").delay(200).fadeIn(500); $("#bdy-cnt4").hide();} else {$bx4.addClass("grid20_60");};
      if ($bx2.hasClass("grid60_20")){$bx2.removeClass("grid60_20"); $bx2.addClass("grid20_60"); $("#start-cnt2").delay(200).fadeIn(500); $("#bdy-cnt2").hide();} else {$bx2.addClass("grid20_60");};};
      $("#start-cnt3").hide();
      $("#bdy-cnt3").delay(900).fadeIn(500);
      setTimeout(bx3ResetClass, 100);
  } else if ($("#ben_bx3").hasClass("grid60_20")) {
      function bx3RemoveClass() {
      $bx1.removeClass("grid20_60");
      $bx2.removeClass("grid20_60");
      $bx4.removeClass("grid20_60");
      $bx3.removeClass("grid60_20");};
      $("#start-cnt3").delay(200).fadeIn(500);
      $("#bdy-cnt3").hide();
      setTimeout(bx3RemoveClass, 1);
  } else {
    function bx3AddClass (){
     $bx1.addClass("grid20_60");
     $bx2.addClass("grid20_60");
     $bx4.addClass("grid20_60");
     $bx3.addClass("grid60_20");
     $("#start-cnt3").hide();
     $("#bdy-cnt3").delay(100).fadeIn(500);
   };
     setTimeout(bx3AddClass, 1);
  };
});
$( "#ben_bx4" ).click(function() {
  if ($bx4.hasClass("grid20_60")) {
      function bx4ResetClass (){
      $bx4.removeClass("grid20_60");
      $bx4.addClass("grid60_20");
      if ($bx1.hasClass("grid60_20")){$bx1.removeClass("grid60_20"); $bx1.addClass("grid20_60"); $("#start-cnt1").delay(200).fadeIn(500); $("#bdy-cnt1").hide();} else {$bx1.addClass("grid20_60");};
      if ($bx3.hasClass("grid60_20")){$bx3.removeClass("grid60_20"); $bx3.addClass("grid20_60"); $("#start-cnt3").delay(200).fadeIn(500); $("#bdy-cnt3").hide();} else {$bx3.addClass("grid20_60");};
      if ($bx2.hasClass("grid60_20")){$bx2.removeClass("grid60_20"); $bx2.addClass("grid20_60"); $("#start-cnt2").delay(200).fadeIn(500); $("#bdy-cnt2").hide();} else {$bx2.addClass("grid20_60");};};
      $("#start-cnt4").hide();
      $("#bdy-cnt4").delay(900).fadeIn(500);
      setTimeout(bx4ResetClass, 100);
  } else if ($("#ben_bx4").hasClass("grid60_20")) {
      function bx4RemoveClass() {
      $bx1.removeClass("grid20_60");
      $bx2.removeClass("grid20_60");
      $bx3.removeClass("grid20_60");
      $bx4.removeClass("grid60_20");};
      $("#start-cnt4").delay(200).fadeIn(500);
      $("#bdy-cnt4").hide();
      setTimeout(bx4RemoveClass, 1);
  } else {
    function bx4AddClass (){
     $bx1.addClass("grid20_60");
     $bx2.addClass("grid20_60");
     $bx3.addClass("grid20_60");
     $bx4.addClass("grid60_20");
     $("#start-cnt4").hide();
     $("#bdy-cnt4").delay(100).fadeIn(500);
   };
     setTimeout(bx4AddClass, 1);
  };
})
});

// END SECTION THREE

// SECTION FOUR
var myPlayer = videojs('example_video_1');
$("#close4").click(function(){
$("#section4").fadeOut( 500, function(){
    $("#body4").fadeOut();
    if (myPlayer.paused()) {
        myPlayer.paused();
      }
      else {
        myPlayer.pause();
      }
} );
$(".bg-fade").fadeTo(0, 0);
$("h1").fadeTo(0, 0);
$(".home_logo").fadeTo(0,0)
$("#home").delay(500).fadeIn( 900, function(){
  buttonHeight();
  $(".bg-fade").fadeTo(0, 1);
  $("h1").fadeTo(0, 1);
  $(".main-wrp").addClass("bounceIn");
  $(".home_logo").fadeTo(200,1)

});

});

$("#section4btn").click(function() {
  $("#section4").show();
  $("#home").fadeOut(300, function(){
      $(this).hide()
    });
    $(".main-wrp").removeClass("bounceIn");
    $("#body4").delay (100).fadeIn(400);
     videojs.options.flash.swf = "video-js.swf";
    if (myPlayer.paused()) {
        myPlayer.play();
      }
      else {
        myPlayer.pause();
      }
    function HeaderBodyHeight () {
  $(".header").each(function(){
    headHeight = $("#header5").outerHeight();
    bdyHeight = $(window).height()-headHeight;
    $('.body').css('height', bdyHeight);
    //console.log (headHeight, bdyHeight)
     //console.log (headHeight)
  });
}
 HeaderBodyHeight();
 $(window).resize(function(){

  HeaderBodyHeight();
});
    });
// END SECTION FOUR

// SECTION FIVE
$("#close5").click(function(){
$('html, body').css('overflow' , 'auto');
$("#section5").fadeOut( 500, function(){
   $("#header5").delay(400).removeClass("hdSlide");
    $("#body5").fadeOut();
} );
$(".bg-fade").fadeTo(0, 0);
$("h1").fadeTo(0, 0);
$(".home_logo").fadeTo(0,0)
$("#home").delay(500).fadeIn( 900, function(){
  buttonHeight();
  $(".bg-fade").fadeTo(0, 1);
  $("h1").fadeTo(0, 1);
  $(".main-wrp").addClass("bounceIn");
  $(".home_logo").fadeTo(200,1)
});


});

$("#section5btn").click(function() {
  $("#section5").show();
  $("#home").fadeOut(300, function(){
    $(this).hide()
  });
  $(".main-wrp").removeClass("bounceIn");
  $("#header5").fadeIn().addClass('hdSlide');
  $('html, body').css('overflow', 'hidden');
  $("#body5").delay (900).fadeIn(400);
   function HeaderBodyHeight () {
  $(".header").each(function(){
    headHeight = $("#header5").outerHeight();
    bdyHeight = $(window).height()-headHeight;
    $('.body').css('height', bdyHeight);
    //console.log (headHeight, bdyHeight)
     //console.log (headHeight)
  });
}
 HeaderBodyHeight();
 $(window).resize(function(){

  HeaderBodyHeight();
});

 $("#flipPad1 a:not(.revert1)").bind("click",function(){
    //if ($("#flipPad1").hasClass("hideopen")){$("#flipPad1").removeClass("hideopen");};
    if ($("#flipPad1").hasClass("hideclose")){$("#flipPad1").removeClass("hideclose").addClass('hideopen');} else {$("#flipPad1").addClass('hideopen')};

    var $this = $(this);
    $("#flipbox1").flip({

      //direction: $this.attr("rel"),
      color: $this.attr("rev"),
      content: $this.attr("title"),//(new Date()).getTime(),
      //onBefore: function(){$(".revert").show()}
    })
    return false;
  });

  $(".revert1").bind("click",function(){
    if ($("#flipPad1").hasClass("hideopen")){$("#flipPad1").removeClass("hideopen").addClass('hideclose');} else {$("#flipPad1").addClass('hideclose')};
    $("#flipbox1").revertFlip();
    return false;
  });

  $("#flipPad2 a:not(.revert2)").bind("click",function(){
    if ($("#flipPad2").hasClass("hideclose")){$("#flipPad2").removeClass("hideclose").addClass('hideopen');} else {$("#flipPad2").addClass('hideopen')};
    var $this = $(this);
    $("#flipbox2").flip({

      //direction: $this.attr("rel"),
      color: $this.attr("rev"),
      content: $this.attr("title"),//(new Date()).getTime(),
      //onBefore: function(){$(".revert").show()}
    })
    return false;
  });

  $(".revert2").bind("click",function(){
    if ($("#flipPad2").hasClass("hideopen")){$("#flipPad2").removeClass("hideopen").addClass('hideclose');} else {$("#flipPad2").addClass('hideclose')};
    $("#flipbox2").revertFlip();
    return false;
  });

  $("#flipPad3 a:not(.revert3)").bind("click",function(){
    if ($("#flipPad3").hasClass("hideclose")){$("#flipPad3").removeClass("hideclose").addClass('hideopen');} else {$("#flipPad3").addClass('hideopen')};
    var $this = $(this);
    $("#flipbox3").flip({

      //direction: $this.attr("rel"),
      color: $this.attr("rev"),
      content: $this.attr("title"),//(new Date()).getTime(),
      //onBefore: function(){$(".revert").show()}
    })
    return false;
  });

  $(".revert3").bind("click",function(){
    if ($("#flipPad3").hasClass("hideopen")){$("#flipPad3").removeClass("hideopen").addClass('hideclose');} else {$("#flipPad3").addClass('hideclose')};
    $("#flipbox3").revertFlip();
    return false;
  });


});

// END SECTION FIVE


function buttonHeight (){
$(".tpBTN").each(function(){
  btnHeight = $(this).width()*.492;
  $(this).css('height', btnHeight);
});
$(".btBTN").each(function(){
  btnHeight = $(this).width()*.75;
  $(this).css('height', btnHeight);
});
}

$(window).load(function() {
  buttonHeight();

}).resize();
setTimeout(buttonHeight, 900);
$(window).resize(function(){
  buttonHeight();

});
