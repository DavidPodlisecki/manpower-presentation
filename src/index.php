<!DOCTYPE>
<html lang="en">
<head>
  <!-- <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title> -->
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>RightEverywhere &reg;</title>
  <meta name="description" content="RightEverywhere Home">
  <meta name="author" content="Manpower">
  <link href="includes/css/vendor.css" rel="stylesheet" />
  <link rel="stylesheet" href="includes/css/style.css">
  <script src="includes/js/modernizr.custom.80936.js"></script>
  <!--[if lt IE 9]>
  <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
  <![endif]-->
</head>

<body id="dashboard">
  <!-- START HOME -->
  <?php include('sections/home-section.php');?>
  <!-- END HOME -->

  <!-- START SECION 1 | WHAT IS RIGHTEVERYWHERE ? -->
  <?php include('sections/section1.php');?>
  <!-- END SECION 1 | WHAT IS RIGHTEVERYWHERE ? -->

  <!-- START SECION 2 | WHAT IS RIGHTEVERYWHERE ? -->
  <?php include('sections/section2.php');?>
  <!-- END SECION 2 | WHAT IS RIGHTEVERYWHERE ? -->

  <!-- START SECION 3 | WHAT IS RIGHTEVERYWHERE ? -->
  <?php include('sections/section3.php');?>
  <!-- END SECION 3 | WHAT IS RIGHTEVERYWHERE ? -->

  <!-- START SECION 4 | WHAT IS RIGHTEVERYWHERE ? -->
  <?php include('sections/section4.php');?>
  <!-- END SECION 4 | WHAT IS RIGHTEVERYWHERE ? -->

  <!-- START SECION 5 | WHAT IS RIGHTEVERYWHERE ? -->
  <?php include('sections/section5.php');?> 
  <!-- END SECION 5 | WHAT IS RIGHTEVERYWHERE ? -->

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
  <script src="includes/js/flowtype.js"></script>
  <!-- bxSlider Javascript file -->
  <script src="includes/js/jquery.bxslider.min.js"></script>
  <script src="includes/js/jquery.flip.min.js"></script>
  <script src="includes/js/jquery-ui.js"></script>
  <script src="includes/js/video.js"></script>
  <script src="includes/js/main.js"></script>
</body>
</html>
