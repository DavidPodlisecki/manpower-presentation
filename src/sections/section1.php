<div id="section1">
  <div id="header1" class="header transition-header">
    <div class="p1-0">
     <span class="icon icon-roundInfo orange left"></span>
            <p>What is RightEverywhere<sup class="sup">&reg;</sup>?</p>
            <button id="close1" class="right"><span class="icon icon-close"></span></button>
    </div>
  </div>
  <div id="body1" class="body bod1">
    <div class="cntBX-left">
      <div class="p1 h100-1">
      <div class="bgLT-orange h100 center-container">
        <div class="center-area">
          <div class="centered">
            <h3 class="orangeBorder_b">An Innovative Career<br/> Management Platform</h3>
            <p>RightEverywhere<sup class="sup">&reg;</sup> guides users on their professional journey by helping them discover their strengths and providing them with the right tools and resources for career success.</p>
          </div>
        </div>
      </div>
    </div>
    </div>
    <div class="cntBX-right">
     <div class="bxSlider_wrapper" id="whatIsSlider">
      <ul class="bxslider">
        <li>
          <div class="sldr_img"><img src="includes/images/screen_sliderPlaceholder.png"></div>
          <div class="sldr_txt">
            <p>Achieve the best career outcomes<br/> in the shortest time possible</p>
          </div>
        </li>
        <li>
          <div class="sldr_img"><img src="includes/images/screen_sliderPlaceholder2.png"></div>
          <div class="sldr_txt">
            <p>Understand individuals’ strengths and career<br/> potential through interactive assessments</p>
          </div>
        </li>
        <li>
          <div class="sldr_img"><img src="includes/images/screen_sliderPlaceholder3.png"></div>
          <div class="sldr_txt">
            <p>Personalized feedback unique to each<br/>career journey</p>
          </div>
        </li>
      </ul>
    </div>
  </div>
  </div>
</div>
