<div id="section5">
  <div id="header5" class="header transition-header">
    <div class="p1-0">
     <span class="icon icon-roundWho green left"></span>
            <p>Who is Right Management?</p>
            <button id="close5" class="right"><span class="icon icon-close"></span></button>
    </div>
  </div>
  <div id="body5" class="body">
    <div class="cntBX-left">
      <div class="p1 h100-1">
        <div class="bgLT-gray woisSbTop center-container">
          <div class="center-area">
            <div class="centered whoisLogo">

            </div>
          </div>
        </div>
        <div class="bgLT-green woisSbBot center-container">
          <div class="center-area">
            <div class="centered">
              <p>Right Management is the global career and talent development expert within ManpowerGroup. We help organizations become more agile, attractive and innovative by creating a culture of career management and learning that nurtures future talent, motivates and engages people, and provides individuals with opportunities to increase their value throughout their careers.</p>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="cntBX-right">
      <div class="p1-1-0-0 h100-1-1">
        <div class="h1_3">
          <div class="flipBx1" style="position:relative;">
            <div class="h100 bgLT-blue" id="flipbox1" style="visibility: visible;">
              <h5>ORGANIZATIONAL EFFECTIVENESS</h5>
            </div>
            <div id="flipPad1" class="hideclose">
              <a href="#" class="top1" rel="bt" rev="#6ea9ef" title="<span><h6>ORGANIZATIONAL EFFECTIVENESS</h6>Right Management works with your team to develop strong leadership pipelines, meaningful workflow, and a career-focused culture that attracts and retains talent. We’ll identify solutions to keep your employees more engaged while maximizing their productivity.<span>"><span class="icon icon-open"></span></a>
              <a href="#" class="revert1" style="display: inline;"><span class="icon icon-close"></span></a>
            </div>
          </div>
        </div>
        <div class="h1_3">
          <div class="flipBx2" style="position:relative;">
            <div class="h100 bgLT-red" id="flipbox2" style="visibility: visible;">
              <h5>individual development</h5>
            </div>
            <div id="flipPad2" class="hideclose">
              <a href="#" class="top2" rel="bt" rev="#dd6370" title="<span><h6>individual development</h6>Right Management provides tailored assessment, development and coaching solutions to improve capabilities, strengthen leader pipeline and accelerate employee time to value.<span>"><span class="icon icon-open"></span></a>
              <a href="#" class="revert2" style="display: inline;"><span class="icon icon-close"></span></a>
            </div>
          </div>
        </div>
        <div class="h1_3">
          <div class="flipBx3" style="position:relative;">
            <div class="h100 bgLT-orange" id="flipbox3" style="visibility: visible;">
              <h5>career management</h5>
            </div>
            <div id="flipPad3" class="hideclose">
              <a href="#" class="top3" rel="bt" rev="#ec964e" title="<span><h6>career management</h6>Reduce risk around workforce transition and improve talent mobility by leveraging Right Management’s 35 years of experience developing and implementing outplacement and career development solutions.<span>"><span class="icon icon-open"></span></a>
              <a href="#" class="revert3" style="display: inline;"><span class="icon icon-close"></span></a>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</div>
