<div id="section2">
  <div id="header2" class="header transition-header">
    <div class="p1-0">
     <span class="icon icon-roundCheck blue left"></span>
            <p>See RightEverywhere<sup class="sup">&reg;</sup> in Action</p>
            <button id="close2" class="right"><span class="icon icon-close"></span></button>
    </div>
  </div>
  <div id="body2" class="body">
           <!-- slider 2 -->
           <div class="bxSlider_wrapper2" id="actionSlider">
      <ul class="bxslider2">
          <li>
              <div class="fullSliderImg"> <img src="includes/images/slide1.gif"></div>
              <div class="fullSliderTxt">
                Progress is tracked as individuals advance through their journey
              </div>
          </li>
          <li>
              <div class="fullSliderImg"> <img src="includes/images/Slide2.gif"></div>
              <div class="fullSliderTxt">
                Individuals will evaluate their skills, strengths and interests to discover a career that will be rewarding for them
              </div>
          </li>
          <li>
              <div class="fullSliderImg"> <img src="includes/images/Slide3.gif"></div>
              <div class="fullSliderTxt">
                Identify values to discover what individuals are looking for in a career
              </div>
          </li>
          <li>
              <div class="fullSliderImg"> <img src="includes/images/slide4.gif"></div>
              <div class="fullSliderTxt">
                Perfect your own 30 second commercial, practice interviewing and receive valuable feedback using our interviewing tools
              </div>
          </li>
      </ul>
    </div>
  </div>
</div>
