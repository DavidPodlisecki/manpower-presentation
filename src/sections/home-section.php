<div id="home">
  <div class="main-wrp box fade-in one">
    <h1>Achieve Career Success with RightEverywhere<sup class="sup">&reg;</sup></h1>
    <div class="section group">
      <div  id="section1btn" class="bg-fade col span_1_of_2 tpBTN">
        <div class="outer">
         <div class="middle">
         <div class="inner">
          <span class="icon icon-info"></span>
          <p>What is RightEverywhere<sup class="sup">&reg;</sup>?</p>
        </div>
      </div>
    </div>
      </div>
      <div id="section2btn" class="bg-fade col span_1_of_2 tpBTN">
        <div class="outer">
         <div class="middle">
         <div class="inner">
          <span class="icon icon-check"></span>
          <p>See RightEverywhere<sup class="sup">&reg;</sup> in action</p>
        </div>
      </div>
    </div>
  </div>
    </div>
    <div class="section group">
      <div id="section3btn" class="bg-fade col span_1_of_3 btBTN">
         <div class="outer">
         <div class="middle">
         <div class="inner">
          <span class="icon icon-graph"></span>
          <p>What are the benefits?</p>
        </div>
        </div>
        </div>
      </div>
      <div id="section4btn"  class="bg-fade col span_1_of_3 btBTN">
       <div class="outer">
         <div class="middle">
         <div class="inner">
          <span class="icon icon-play"></span>
          <p>Watch the video</p>
        </div>
      </div>
    </div>
      </div>
      <div id="section5btn" class="bg-fade col span_1_of_3 btBTN">
        <div class="outer">
         <div class="middle">
         <div class="inner">
          <span class="icon icon-who"></span>
          <p>Who is Right Management?</p>
        </div>
      </div>
    </div>
      </div>
    </div>
  </div>
  <div class="home_logo">
    <img id="logo" src="images/white_logo.png" alt="Right Mangement Logo">
  </div>
</div>
