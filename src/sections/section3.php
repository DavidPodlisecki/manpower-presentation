<div id="section3">
  <div id="header3" class="header transition-header">
    <div class="p1-0">
     <span class="icon icon-roundGraph red left"></span>
            <p>Benefits</p>
            <button id="close3" class="right"><span class="icon icon-close"></span></button>
    </div>
  </div>
  <div id="body3" class="body">
    <div id="ben_bx1">
      <div class="p-5 h100_5 w100-1 textsize">
        <div class="bgLT-blue h100 center-container">
          <div class="start-cnt"  id="start-cnt1">
            <div class="center-area">
              <div class="centered center-txt">
              <div class="start-cnt-wrp">
                <div class="ben-icon">
                  <span class="icon icon-compas"></span>
                </div>
                SELF<br/>DISCOVERY
                <div class="ben-open">
                  <span class="icon icon-open"></span>
                </div>
              </div>
              </div>
            </div>
          </div>
          <div class="bdy-cnt" id="bdy-cnt1">
            <span class="icon icon-close bdy-close"></span>
            <div class="ben-bdy-title">
            <span class="icon icon-compas"></span>
            <h4>SELF DISCOVERY</h4>
            </div>
            <div class="ben-bdy-cnt">
              <ul>
                <li>
                  <h5>guided user EXPERIENCE</h5>
                  Tools and resources tailored to your interests and needs.
                </li>
                <li>
                  <h5>Insightful assessments</h5>
                  Detailed results for your ongoing career journey.
                </li>
                <li>
                  <h5>Your goals, your skills, yourself</h5>
                  Learn more about your strengths and opportunities to reach your goals.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="ben_bx2">
      <div class="p-5-5_0 h100_5 w100-1 textsize">
        <div class="bgLT-red h100 center-container">
           <div class="start-cnt"  id="start-cnt2">
             <div class="center-area">
              <div class="centered center-txt">
                <div class="start-cnt-wrp">
                <div class="ben-icon">
                  <span class="icon icon-stamp"></span>
                </div>
                PERSONAL<br/>DEVELOPMENT <br/>&amp; BRANDING
                <div class="ben-open">
                  <span class="icon icon-open"></span>
                </div>
              </div>
              </div>
            </div>
          </div>
          <div class="bdy-cnt" id="bdy-cnt2">
            <span class="icon icon-close bdy-close"></span>
            <div class="ben-bdy-title">
            <span class="icon icon-stamp"></span>
            <h4>PERSONAL DEVELOPMENT <br/> &amp; BRANDING</h4>
            </div>
            <div class="ben-bdy-cnt">
             <ul>
                <li>
                  <h5>ONE-ON-ONE CAREER COACH</h5>
                  Gain detailed feedback from your personal career coach.
                </li>
                <li>
                  <h5>LEARN NEW SKILLS</h5>
                  Develop your expertise through our comprehensive training portal.
                </li>
                <li>
                  <h5>leverage social networking</h5>
                  Harness the power of online networking sites with our expert tips to maximize your personal brand.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="ben_bx3">
      <div class="p-5-0_5 h100_5 w100-1 textsize">
        <div class="bgLT-orange h100 center-container">
          <div class="start-cnt" id="start-cnt3">
           <div class="center-area">
            <div class="centered center-txt">
              <div class="start-cnt-wrp">
              <div class="ben-icon">
                <span class="icon icon-tools"></span>
              </div>
              TOOLS FOR<br/>SUCCESS
              <div class="ben-open">
                  <span class="icon icon-open"></span>
                </div>
            </div>
            </div>
          </div>
        </div>
          <div class="bdy-cnt" id="bdy-cnt3">
            <span class="icon icon-close bdy-close"></span>
            <div class="ben-bdy-title">
            <span class="icon icon-tools"></span>
            <h4>TOOLS FOR SUCCESS</h4>
            </div>
            <div class="ben-bdy-cnt">
            <ul>
                <li>
                  <h5>Get ahead in interviews</h5>
                  Innovative tools to simulate real world interviewing.
                </li>
                <li>
                  <h5>COMPREHENSIVE EDUCATIONAL RESOURCES</h5>
                  Leverage our rich collection of educational resources and tools curated by our experts.
                </li>
                <li>
                  <h5>Exclusive perks</h5>
                  Special events, webinars and networking sessions only for our candidates.
                </li>
                <li>
                  <h5>extensive job databases</h5>
                  Access to exclusive listings and the chance to find out about jobs before anyone else.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div id="ben_bx4">
      <div class="p-5 h100_5 w100-1 textsize">
        <div class="bgLT-green h100 center-container">
          <div class="start-cnt" id="start-cnt4">
           <div class="center-area">
            <div class="centered center-txt">
              <div class="start-cnt-wrp">
              <div class="ben-icon">
                <span class="icon icon-technology"></span>
              </div>
              INDUSTRY-<br/>LEADING<br/> TECHNOLOGY
              <div class="ben-open">
                  <span class="icon icon-open"></span>
                </div>
            </div>
            </div>
          </div>
        </div>
          <div class="bdy-cnt" id="bdy-cnt4">
            <span class="icon icon-close bdy-close"></span>
            <div class="ben-bdy-title">
            <span class="icon icon-technology"></span>
            <h4>INDUSTRY-LEADING TECHNOLOGY</h4>
            </div>
            <div class="ben-bdy-cnt">
            <ul>
                <li>
                  <h5>Customize Your Platform </h5>
                  Dynamic customization options empower our clients to align RightEverywhere<sup class="sup">&reg;</sup> with their brand and program
                </li>
                <li>
                  <h5>Take RightEverywhere<sup class="sup">&reg;</sup> Anywhere</h5>
                  Comprehensive and globally consistent, our responsive digital site can be accessed seamlessly between desktop, tablets and smart phones.
                </li>
                <li>
                  <h5>SHowcase Dynamic, Engaging content</h5>
                  From online webinar to video interviewing, RightEverywhere<sup class="sup">&reg;</sup> is designed to help users achieve their professional goals.
                </li>
                <li>
                  <h5>Provide Best-In-Class Convenience</h5>
                  RightEverywhere<sup class="sup">&reg;</sup> is WCAG compliant and is easily Accessible by user with disabilities.
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
