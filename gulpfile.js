const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const sass        = require('gulp-sass');
const rename = require('gulp-rename');
const phpipe = require('gulp-phpipe');
// const cleanCSS = require('gulp-clean-css');
// Compile Sass & Inject Into Browser
gulp.task('sass', function() {
    return gulp.src(['src/scss/*.scss'])
        .pipe(sass())
        .pipe(gulp.dest("src/dist/includes/css"))
        .pipe(browserSync.stream());
});
// gulp.task('minify-css', function () {
//     //gulp.src('src/dist/includes/css/*.css')
//         // .pipe(cssmin())
//         // .pipe(rename({suffix: '.min'}))
//         // .pipe(gulp.dest('src/dist/includes/css'));
//
//     gulp.src('src/dist/includes/css/*.css')
//     .pipe(cleanCSS({compatibility: 'ie8'}))
//     .pipe(gulp.dest('src/dist/includes/css/min_css'));
// });
gulp.task('static-generator', function() {

  gulp.src('src/*.php')
      .pipe(phpipe())
      .pipe(rename(function (path) {
          path.extname = ".html";
          return path;
      }))
      .pipe(gulp.dest('src/dist/'));
});

// Watch Sass & Serve
gulp.task('serve', ['sass'], function() {
    browserSync.init({
        server: "./src/dist"
    });
    gulp.watch(['src/scss/*.scss'], ['sass']);
    gulp.watch( [ 'src/*.php' ], [ 'static-generator' ] );
    // gulp.watch( [ 'src/scss/*.scss' ], [ 'minify-css' ] );
    gulp.watch("src/dist/*.html").on('change', browserSync.reload);

});



// Default Task
gulp.task('default', ['serve','static-generator']);
